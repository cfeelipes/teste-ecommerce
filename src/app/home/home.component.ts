import { Card } from './../models/card';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FormDialogComponent } from '../form-dialog/form-dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  card: Card;

  constructor(private dialog: MatDialog) { }

  ngOnInit() { }

  openCardAddDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(FormDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(data => {
      console.log('Dialog output: ', data);
      this.card = data;
    });
  }
}