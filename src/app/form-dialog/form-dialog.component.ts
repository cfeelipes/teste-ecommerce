import { Card } from './../models/card';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-dialog',
  templateUrl: './form-dialog.component.html',
  styleUrls: ['./form-dialog.component.sass']
})
export class FormDialogComponent implements OnInit {

  cardForm: FormGroup;
  fileName: any;
  fileUploaded: any;

  constructor(private fb: FormBuilder, private dialogRef: MatDialogRef<FormDialogComponent>) { }

  ngOnInit() {
    this.cardForm = this.fb.group({
      name: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
      image: ['']
    });
  }

  save() {
    this.dialogRef.close(this.cardForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  uploadFile($event: any) {
    this.fileUploaded = $event.target.files[0];
    this.fileName = this.fileUploaded.name;
  }

  readFile(imageFile: any): void {
    const image = imageFile;
    const reader = new FileReader();
    reader.readAsDataURL(image);
    reader.onload = function () {
      console.log(reader.result);
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }
}
